import os
import shutil
import sys


def copy_tree(path, dest_folder: str):
    for (folder, dirnames, filenames) in os.walk(path):
        for d in dirnames:
            fpath = os.path.join(path, d)
            print("Copying folder {}".format(fpath))
            copy_tree(fpath, dest_folder)

        for f in filenames:
            fpath = os.path.join(path, f)
            dpath = os.path.join(dest_folder, f)
            if not os.path.islink(path):
                print("Copying file {} to  {}...".format(fpath, dpath))
                if os.path.exists(dpath):
                    print("Destination file exists, skipping...")
                else:
                    shutil.copy(fpath, dpath)
            else:
                print("Skipped symbolic link {}".format(fpath))


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: python3 copy_scaffold <storage dir> <destination path>")
        sys.exit(0)

    if not os.path.isdir(sys.argv[1]):
        print("Source dir not found!")
        sys.exit(0)

    if not os.path.isdir(sys.argv[2]):
        print("Destination dir not found!")
        sys.exit(0)

    copy_tree(sys.argv[1], sys.argv[2])
